#sample app--------------
library(shiny)
library(shinydashboard)
library(fasttime)
library(lubridate)
library(data.table)
library(dplyr)
library(httr)
library(jsonlite)
library(RCurl)
library(pbapply)
library(openxlsx)
library(readxl)
library(reshape)
library(tidyr)
library(ggmap)
library(leaflet)
library(raster)
library(sf)
library(rgdal)
library(leaflet.extras)
library(remotes)
library(stringi)
library(rgeos)
library(rmapshaper)
library(sp)
library(echarts4r)
library(shinydashboardPlus)
library(shinycssloaders)
library(DT)
library(mapview)
library(scales)
library(zoo)
library(shinyWidgets)
library(Hmisc)
library(RODBC)
library(data.table)
library(shinyalert)
library(plotly)
library(rintrojs)

main = getwd()
core = c(19,22,24,20,18,17,14,10,13,6,9,1,5,7)

#precinct borough designations
boro_key = 
rbind(
data.table(
Precinct= c(60,61,62,63,66,67,68,69,70,71,72,73,75,76,77,78,79,81,83,84,88,90,94)
)[,borough:="brooklyn"],

data.table(
Precinct = c(100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115)   
)[,borough:="queens"],


data.table(
Precinct=c(120,121,122,123)
)[,borough:="staten_island"],

data.table(
Precinct=c(40,41,42,43,44,45,46,47,48,49,50,52)  
)[,borough:="bronx"],

data.table(
Precinct=c(1,5,6,7,9,10,13,14,17,18,19,20,22,23,24,25,26,28,30,32,33,34)
)[,borough:="manhattan"]
)


#pull icons------
strike_icons = makeIcon(
  iconUrl = "https://img.icons8.com/color/48/000000/flash-bang.png",
  iconWidth = 18, iconHeight = 18,
  iconAnchorX = 18, iconAnchorY = 18
)

#prep data------------
afg_gc=fread(paste0(main,"/data/data.csv"))

#by violation precinct
aggs = afg_gc[,.(total_violations = .N)
            ,.(the_date = issue_date_f,
               Precinct = violation_precinct, county)]

#unique vehicles by year month
uv = 
  unique(afg_gc, by = c("plate_id","issue_date"))[
    ,.(total_vehicles = uniqueN(plate_id))
    ,.(year_month = substr(issue_date_f,1,7)
    )][,year_month:=as.Date(paste0(year_month,"-01"))]

#unique vehicles by year month and registration
uv_state = 
  unique(afg_gc, by = c("plate_id","issue_date"))[
    ,.(total_vehicles = uniqueN(plate_id))
    ,.(
      registration_state
    )]

#by issuing precinct
aggs_issue = afg_gc[,.(total_violations = .N)
            ,.(the_date = issue_date_f,
               Precinct = issuer_precinct, county)]

#violations by type and year_month
aggs_vio_type = afg_gc[,.(total_violations = .N,
                        total_cost = sum(final_cost, na.rm = T)
                        )
                     ,.(year_month = substr(issue_date_f,1,7),
                        Precinct = violation_precinct,
                        Precinct_i = issuer_precinct,
                        violation_desc,
                        issuing_agency,
                        county)][,year_month:=as.Date(paste0(year_month,"-01"))]


#first an aggregated table by date

#recode vehicle types
vt_recode = c("CMB", "COM", "HAM", "HIS", "OMR",
              "OMS", "OMT", "OMV","ORC", "ORG",
              "PHS", "PSD","RGL", "SPO", "SRF",
              "TOW", "TRL", "VAS")
afg_gc[,plate_type:=ifelse(
  !(plate_type %in% vt_recode),
  plate_type,
  "OTHER"
  )][plate_type=='999',plate_type:='Unknown']


veh_type_agg = 
  afg_gc[,.(total_violations = .N,
            total_cost = sum(final_cost, na.rm = T))
         ,.(plate_type,
            Precinct = violation_precinct,
            Precinct_i = issuer_precinct,
            violation_desc,
            registration_state,
            issuing_agency,
            year_month = substr(issue_date_f,1,7)
         )][,year_month:=as.Date(paste0(year_month,"-01"))][registration_state==99
                                                             ,registration_state:='OTHER'][order(plate_type)]


veh_type_agg = merge(veh_type_agg,uv, by = "year_month", all.x = T)

#merge borough 
veh_type_agg = merge(veh_type_agg, boro_key, by = "Precinct", all.x = T)
#veh_type_agg = merge(veh_type_agg, boro_key, by = "Precinct", all.x = T)




#prep shape file---------------
#setwd(paste0(main,"/Afghanistan_Districts"))
shape = readOGR(dsn=path.expand("geo/nypp.shp")
                ,layer = "nypp") 


shape = spTransform(shape, CRS("+proj=longlat +datum=WGS84"))
#colnames(shape@data)[3] = "district"

server = 
  function(input, output, session) {
    
    observeEvent(input$help,{
      introjs(session)
    })
    
    #all data pulls------------------------------
    
    #value box data
    for_vb = reactive({
      start = as.Date(input$input_date[1])
      end = as.Date(input$input_date[2])
      veh_type_agg[year_month > start & year_month <= end
                   ,.(total_violations = sum(total_violations, na.rm = T)
                      ,total_cost = sum(total_cost, na.rm = T))
                   ,.(Precinct, violation_desc)] 
    })
    
    #vehicle violations by state
    for_vs = reactive({
      start = as.Date(input$input_date[1])
      end = as.Date(input$input_date[2])
      veh_type_agg[year_month > start & year_month <= end]
    })
    
    #vehicles committing violations by state
    for_uv_state = reactive({
      start = as.Date(input$input_date[1])
      end = as.Date(input$input_date[2])
      unique(afg_gc[issue_date_f > start & issue_date_f <= end]
             , by = c("plate_id","issue_date"))[
        ,.(total_vehicles = uniqueN(plate_id))
        ,.(
          registration_state
        )][order(total_vehicles, decreasing = T)]
    })
    
    #data for issue agency pie chart
    for_issue_agency = reactive({
      start = as.Date(input$input_date[1])
      end = as.Date(input$input_date[2])
      veh_type_agg[year_month > start & year_month <= end
                   ,.(total_violations = sum(total_violations, na.rm = T)
                      )
                   ,.(issuing_agency)] 
    })
    
    
    #value boxes----------------------------------
    output$box1 = renderValueBox({
        valueBox(
          sum(for_vb()$total_cost), "Total Money Collected", icon = icon("fas fa-money-bill-wave"),
        color = "yellow")
    }) 
    output$box2 = renderValueBox({
        valueBox(
          sum(for_vb()[Precinct %in% core]$total_cost), "Total Core Money Collected", icon = icon("fas fa-money-bill-wave"),
        color = "green")
    }) 
    output$box3 = renderValueBox({ 
         valueBox(
           for_vb()[
             ,.(q = sum(total_violations))
             ,.(violation_desc)][order(q, decreasing = T)][1]$q
           , paste0("Highest Violation - ",
                    for_vb()[
                      ,.(q = sum(total_violations))
                      ,.(violation_desc)][order(q, decreasing = T)][1]$violation_desc
                    )
           , icon = icon("fas fa-money-bill-wave"),
        color = "maroon")
    })
    output$box4 = renderValueBox({ 
            valueBox(
          sum(for_vb()$total_violations), "Total Number of Violations", icon = icon("fas fa-id-card"),
        color = "maroon")
    })
    
    
    
    #m1------------------------------------------------------------------------------------------
    
    
    #prep map data---------------------------------------------
    
    
    for_map_r = reactive({
      
      req(input$vo)
      print(input$geography)
      print(input$vo)
      print(input$metric)
      
      if(input$geography!='city' & input$vo =='All') {
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        my_var = input$metric
        data1 = veh_type_agg[#violation_desc == input$vo &
                        borough == input$geography &
                        year_month > start & 
                        year_month <= end
                      ,.(year_month,
                         Precinct,
                         my_var = get(input$metric)
                      )]
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct)] #aggregate before merging
        setnames(data1, "q", my_var) #rename input metric
        
      } else if(input$geography!='city' & input$vo !='All') { 
        
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        my_var = input$metric
        data1 =veh_type_agg[violation_desc == input$vo &
                       borough == input$geography &
                       year_month > start & 
                       year_month <= end
                      ,.(year_month,
                        Precinct,
                        my_var = get(input$metric)
                        )]
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct)] #aggregate before merging
        setnames(data1, "q", my_var) #rename input metric
        
      } else if(input$geography=='city' & input$vo !='All') {
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        my_var = input$metric
        data1 =veh_type_agg[violation_desc == input$vo &
                        #borough == input$geography &
                        year_month > start & 
                        year_month <= end
                      ,.(year_month,
                         Precinct,
                         my_var = get(input$metric)
                      )]
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct)] #aggregate before merging
        setnames(data1, "q", my_var) #rename input metric
        
      } else {
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        my_var = input$metric
        data1 =veh_type_agg[#violation_desc == input$vo &
                        #borough == input$geography &
                        year_month > start & 
                        year_month <= end
                      ,.(year_month,
                         Precinct,
                         my_var = get(input$metric)
                      )]
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct)] #aggregate before merging
        setnames(data1, "q", my_var) #rename input metric
      }
      
    })
    
    
    # for_map_v = reactive({
    #   
    #   req(input$vo)
    #   if(input$vo == 'All' & input$geography == 'violation_precinct') {
    #     start = as.Date(input$input_date[1])
    #     end = as.Date(input$input_date[2])
    #     
    #     my_var = input$metric
    #     data1 = aggs_vio_type[year_month > start &
    #                             year_month <= end
    #                           ,.(year_month,
    #                              Precinct,
    #                              my_var = get(input$metric))] #filter and extract data
    #     data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct)] #aggregate before merging
    #     setnames(data1, "q", my_var) #rename input metric
    #     
    #   } else if(input$vo != 'All' & input$geography == 'violation_precinct') {
    #     start = as.Date(input$input_date[1])
    #     end = as.Date(input$input_date[2])
    #     print(start)
    #     print(end)
    #     my_var = input$metric
    #     data1 = aggs_vio_type[year_month > start &
    #                             year_month <= end &
    #                             violation_desc == input$vo
    #                           ,.(year_month,
    #                              Precinct,
    #                              my_var = get(input$metric))] #filter and extract data
    #     data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct)] #aggregate before merging
    #     setnames(data1, "q", my_var) #rename input metric
    #     
    #   } else if(input$vo == 'All' & input$geography == 'issuer_precinct') {
    #     
    #     start = as.Date(input$input_date[1])
    #     end = as.Date(input$input_date[2])
    #     
    #     my_var = input$metric
    #     data1 = aggs_vio_type[year_month > start &
    #                             year_month <= end
    #                           ,.(year_month,
    #                              Precinct_i,
    #                              my_var = get(input$metric))] #filter and extract data
    #     data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct = Precinct_i)] #aggregate before merging
    #     setnames(data1, "q", my_var) #rename input metric 
    #     
    #   } else {
    #     
    #     start = as.Date(input$input_date[1])
    #     end = as.Date(input$input_date[2])
    #     
    #     my_var = input$metric
    #     data1 = aggs_vio_type[year_month > start &
    #                             year_month <= end
    #                           ,.(year_month,
    #                              Precinct_i,
    #                              my_var = get(input$metric))] #filter and extract data
    #     data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct = Precinct_i)] #aggregate before merging
    #     setnames(data1, "q", my_var) #rename input metric 
    #     
    #   }
    #   
    # }) 
    # 
    
    
    # for_map_i = reactive({
    #   start = as.Date(input$input_date[1])
    #   end = as.Date(input$input_date[2])
    #   print(start)
    #   print(end)
    #   my_var = input$metric
    #   data1 = aggs_issue[the_date > start & the_date <= end,.(the_date, Precinct, my_var = get(input$metric))] #filter and extract data
    #   data1 = data1[,.(q = sum(my_var,na.rm = T)),.(Precinct)] #aggregate before merging
    #   setnames(data1, "q", my_var) #rename input metric
    # }) 
    
    
    #render base leaflet map----------------------------------
    
    
    output$leaf = renderLeaflet({
      #----------------------------------------------------- test
      # start = as.Date(input$input_date[1])
      # end = as.Date(input$input_date[2])
      # print(start)
      # print(end)
      # data1 = subset(trips,(the_date > start & the_date <= end))
      # names(data1) = c('the_date','id','count')
      # #print(data1)
      #----------------------------------------------------
      leaflet(options = leafletOptions(zoomControl = FALSE)) %>%
        setView(-73.809354, 40.737084, zoom = 10)%>% 
        addProviderTiles(providers$Esri.WorldImagery, group = 'Satellite') %>%
        addProviderTiles(providers$CartoDB.Positron, group = 'Standard Gray') %>%
        addTiles(group  = 'City View') %>%
        
        addResetMapButton() %>%
        addLayersControl(position = 'topleft',
                         baseGroups = c('Satellite', "Standard Gray", "City View"),
                         options = layersControlOptions(collapsed = FALSE) 
        )
    })
    
    # observe({shinyalert(""
    #                     ,
    #                     "You need to put drones under control; you need to lay out certain rules of engagement
    #                     in order to prevent or minimize collateral casualties.It is extremely important.
    #                     -Vladimir Putin
    #                     "
    #                     , type = "info"
    #                     ,imageUrl = 'https://i.etsystatic.com/16964958/r/il/840a14/1555410384/il_570xN.1555410384_pr6k.jpg'
    #                     ,imageWidth = 400
    #                     ,imageHeight = 400
    #                     ,confirmButtonText = 'Continue')
    # })
    # 
    observe({
      if(input$geography == 'violation_precinct'){
        print(head(for_map_r()))
        my_var = input$metric
        print(my_var)
        pal <- colorNumeric(palette = "OrRd", domain=shape@data[[my_var]])
        shape@data = left_join(shape@data
                               ,for_map_r()
                               , by="Precinct")
        print(head(shape@data))
        leafletProxy("leaf", data = shape) %>%
          clearShapes() %>%
          clearMarkers() %>%
          addPolygons(data  =shape,
                      #fillColor = ~pal(get(my_var)), 
                      weight = 1.3,
                      #opacity = 1,
                      color = ~pal(get(my_var)),
                      #dashArray = "3",
                      fillOpacity = 0.9,
                      popup = paste0("Precinct: ", shape@data$Precinct, "<br>",
                                     my_var,": ", format(shape@data[[my_var]], big.mark = ",")),
                      highlightOptions = highlightOptions(weight = 6,
                                                          color = "orange",
                                                          bringToFront = TRUE))%>%
          #clearControls() %>%
          addLegend("bottomleft"
                    ,pal = colorNumeric(palette = "OrRd"
                                        ,domain=shape@data[[my_var]])
                    ,values = shape@data[[my_var]]
                    ,title = my_var
          )
      } else {
        #do the same thing for now
        print(head(for_map_r(),3))
         my_var = input$metric
       print(my_var)
         pal <- colorNumeric(palette = "OrRd", domain=shape@data[[my_var]])
         shape@data = left_join(shape@data
                                ,for_map_r()
                                , by="Precinct")
         print(head(shape@data))
        leafletProxy("leaf", data = shape) %>%
           clearShapes() %>%
           addPolygons(fillColor = ~pal(get(my_var)), 
                       weight = 2,
                       opacity = 1,
                       color = "white",
                       dashArray = "3",
                       fillOpacity = 0.7,
                       popup = paste0("Precinct: ", shape@data$Precinct, "<br>",
                                      my_var,": ", format(shape@data[[my_var]], big.mark = ",")),
                       highlightOptions = highlightOptions(weight = 6,
                                                           color = "orange",
                                                           bringToFront = TRUE))%>%
           clearControls() %>%
           addLegend("bottomleft"
                     ,pal = colorNumeric(palette = "OrRd"
                                         ,domain=shape@data[[my_var]])
                     ,values = shape@data[[my_var]]
                     ,title = my_var
           )
      }
    })
    
    #create data for lower chart
    for_chart = reactive({
      
      req(input$vo)
      if(input$vo == 'All' & input$geography == 'violation_precinct') {
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        
        my_var = input$metric
        data1 = aggs_vio_type[year_month > start &
                                year_month <= end
                              ,.(year_month,
                                 Precinct,
                                 my_var = get(input$metric))] #filter and extract data
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(year_month)] #aggregate before merging
        setnames(data1, "q",  "total") #rename input metric
        
      } else if(input$vo != 'All' & input$geography == 'violation_precinct') {
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        print(start)
        print(end)
        my_var = input$metric
        data1 = aggs_vio_type[year_month > start &
                                year_month <= end &
                                violation_desc == input$vo
                              ,.(year_month,
                                 Precinct,
                                 my_var = get(input$metric))] #filter and extract data
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(year_month)] #aggregate before merging
        setnames(data1, "q","total") #rename input metric
        
      } else if(input$vo == 'All' & input$geography == 'issuer_precinct') {
        
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        
        my_var = input$metric
        data1 = aggs_vio_type[year_month > start &
                                year_month <= end
                              ,.(year_month,
                                 Precinct_i,
                                 my_var = get(input$metric))] #filter and extract data
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(year_month)] #aggregate before merging
        setnames(data1, "q","total") #rename input metric 
        
      } else {
        
        start = as.Date(input$input_date[1])
        end = as.Date(input$input_date[2])
        
        my_var = input$metric
        data1 = aggs_vio_type[year_month > start &
                                year_month <= end
                              ,.(year_month,
                                 Precinct_i,
                                 my_var = get(input$metric))] #filter and extract data
        data1 = data1[,.(q = sum(my_var,na.rm = T)),.(year_month)] #aggregate before merging
        setnames(data1, "q", "total") #rename input metric 
        
      }
      
    })
    
    
    #for vehicle type pie chart
    for_pie = reactive({
      start = as.Date(input$input_date[1])
      end = as.Date(input$input_date[2])
      
      veh_type_agg = 
      veh_type_agg[year_month > start &
                     year_month <= end
                   ,.(total = sum(total_violations)),.(plate_type)][
                     ,percent:=round(total/sum(total),2)][
                       ,grp:=plate_type][
                         ,labels:=plate_type][
                           ,values:=percent][order(percent, decreasing = T)]
      #veh_type_agg[6:nrow(veh_type_agg),plate_type:='OTHER']
    })
    
    
    #render charts-----------
    
    output$veh_type = renderPlotly({
      
      plot_ly(for_pie(),
                   labels = ~plate_type,
                   values = ~total,
                   type = 'pie') %>%
        layout(title = 'Distribution of Total Violations by Vehicle Type',
               xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
               yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE)
        )
    })
    
    output$issue_agency = renderPlotly({
      print(head(for_issue_agency()))
      
      
      plot_ly(for_issue_agency(),
              labels = ~issuing_agency,
              values = ~total_violations,
              type = 'pie') %>%
        layout(title = 'Distribution of Total Violations by Vehicle Type',
               xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
               yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE)
        )
    })
    
    
    
    output$oot_veh = renderPlotly({
        ec = for_vs()[
          ,.(total_violations = sum(total_violations))
          ,.(registration_state)][order(total_violations, decreasing = T)]
      
        ec = ec[10:nrow(ec)
                ,registration_state:='OTHER'][registration_state==99
                                              ,registration_state:='OTHER'][
            ,.(total_violations = sum(total_violations))
            ,.(registration_state)][order(total_violations, decreasing = T)]
        print(head(ec))
        print(str(ec))
          
          
        ec = plot_ly(ec, x = ~registration_state
                        , y = ~total_violations
                        ,type = 'bar'
                        )
        
        ec = layout(ec,             
                       title = "Total Violations by Vehicle Registration", 
                       xaxis = list(           
                         title = "Registration State",    
                         showgrid = F        
                       ),
                       yaxis = list(          
                         title = "Number of Violations"     
                       ))
        

    })
    
    #number of vehicles violating
    output$veh_vio = renderPlotly({
      print(head(for_uv_state()))
      ec = for_uv_state()[10:nrow(for_uv_state())
              ,registration_state:='OTHER'][
        ,.(total_vehicles = sum(total_vehicles))
        ,.(registration_state)][order(total_vehicles, decreasing = T)]
      
      ec = plot_ly(ec, x = ~registration_state
                   , y = ~total_vehicles
                   ,type = 'bar'
      )
      
      ec = layout(ec,             
                  title = "Total Vehicle Violators by Vehicle Registration", 
                  xaxis = list(           
                    title = "Registration State",    
                    showgrid = F        
                  ),
                  yaxis = list(          
                    title = "Number of Vehicles"     
                  ))
      
    })
    
    
    
    
    
    output$overall_trend = renderPlotly({

      ec = plot_ly(for_chart()[order(year_month)],
              x = ~year_month,
              y = ~total
              , type = 'scatter'
              #, split = ~license_class
              , mode = 'lines'
              #,color = ~license_class
              #,colors = pal
              )
      
      ec = layout(ec,             
                  title = paste(gsub("_"," ",input$metric),"-",tolower(input$vo)), 
                  xaxis = list(           
                    title = "Year and Month",    
                    showgrid = F        
                  ),
                  yaxis = list(          
                    title = "Total"     
                  ))
      
    })
    
    
    #---------------------------------------------------------
    
    # output$moreControls <- renderUI({
    #   tagList(
    #     sliderInput("n", "N", 1, 1000, 500),
    #     textInput("label", "Label")
    #   )
    # })
    
    #--------------------------------------------------------------------------------------------
    
    
    #m2------------------------------------------------------------------------------------------
    
    
    
    output$chooseTime = renderUI({
      selectInput(inputId = "ct"
                  , label = "Choose Your Temperal Element"
                  ,choices = c('Day', 'Week', 'Hour')
                  , selected = 1)
    })
    
    output$chooseVio = renderUI({
      selectInput(inputId = 'vo'
                  , label = "choose violation"
                  , choices = c('All',sort(unique(afg_gc$violation_desc)))
                  , selected = "All")
    })
    
    
    output$chooseCalc = renderUI({
      radioButtons(inputId = "cc"
                   , label = "Choose Your Calculation"
                   ,choices = c('sum', 'avg')
                   , selected = 1)
    })
    
    output$writeText = renderUI({
      textAreaInput(inputId = "wt"
                    , label = "Write in Your SQL"
                    , height = '200%')
    })
    
    
    #render tables---------------------
    
    
    
    #------------------------------------------------------------------------------------------
    
    
    
    
    
    
  }
